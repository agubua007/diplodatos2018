# Instrucciones de uso

1. Descargar el archivo **Dockerfile**
2. Ejecutar el comando **docker build . --tag laboratorio** en la carpeta donde descargo el archivo
3. Ejecutar el comando **docker run -it --rm -p 8888:8888 laboratorio**
4. Una ves que el sistema me indique que el contenedor docker ya esta levantado y corriendo (debe ver una linea similar a esta: **http://localhost:8888/?token=**)
5. Copie y pegue en su browser el link que allí aparece y seleccione el archivo Laboratorio.ipynb
6. Listo!

Adicionalmente si desea acceder a los archivos que se generen adentro de este contenedor, puede asignar un volumen
a una carpeta de sus sistema con este comando

**docker run -it --rm -v [REEMPLAZAR CON PATH DE SU PC]:/home/jovyan/work -p 8888:8888 laboratorio**

Ejemplo de PATH en windows 
C:\Reportes\test

Ejemplo de PATH linux
\tmp\otro

